variable "cluster_name" {
  default = "nginx_poc_ooyala"
}

variable "region" {
  default = "us-east-1"
}

variable availability_zones {
  type    = "list"
  default = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e"]
}

#VPC module input variables
variable vpc_name {
  default = "tf-vpc-nginx-poc"
}

variable cidr {
  default = "10.30.0.0/16"
}

variable env_cidr {
  default = "10.0.0.0/8"
}

variable ssh_key_name {
  default = "nginx-poc"
}

variable public_subnets {
  default = ["10.30.1.0/24", "10.30.2.0/24", "10.30.3.0/24", "10.30.4.0/24", "10.30.5.0/24"]
}

variable enable_dns_support {
  default = "true"
}

variable map_public_ip_on_launch {
  default = "true"
}

variable enable_dns_hostnames {
  default = "true"
}

variable enable_nat_gateway {
  default = "false"
}

variable "environment" {
  default = "nginx-poc-env"
}

variable "ec2_nginx_poc_instance_size" {
  default = "t2.micro"
}

#Ubuntu 16.04
variable "ami" {
  default = "ami-a4c7edb2"
}

#Amazon Linux AMI 2017.03.1.20170623 x86_64 HVM GP2
variable "nginx_ubuntu_ami" {
  default = "ami-a4c7edb2"
}

variable "state_s3_bucket" {
  default = "epam-nginx-terraform-poc"
}

#Space separated emails addresses
variable "critical_alerts_notification" {
  default = "kwame@informatux.net"
}

#Space separated emails addresses
variable "non_critical_alerts" {
  default = "kwame@informatux.net"
}
