#!/bin/bash
yum-config-manager --enable epel
yum -y update
yum -y install git screen httpd tmux awslogs perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https irssi nginx
hostname nginx-ooyala-poc.local
