terraform {
  backend "s3" {
    bucket  = "epam-nginx-terraform-poc"
    key     = "nginx-poc/ooyala-nginx-poc-terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
