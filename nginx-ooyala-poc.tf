resource "aws_instance" "nginx_poc" {
  ami                    = "${var.nginx_ubuntu_ami}"
  instance_type          = "${var.ec2_nginx_poc_instance_size}"
  key_name               = "${var.ssh_key_name}"
  vpc_security_group_ids = ["${aws_security_group.ec2_nginx_poc.id}"]
  subnet_id              = "${module.vpc.public_subnets[0]}"
  user_data              = "${file("files/nginx-ooyala-poc.sh")}"

  #iam_instance_profile        = "${aws_iam_instance_profile.ec2-cloudwatch-test-profile.id}"
  associate_public_ip_address = true
  source_dest_check           = false

  #lifecycle {
  #  ignore_changes = ["user_data"]
  #}

  tags {
    Name          = "nginx test"
    "Terraform"   = "true"
    "Environment" = "${var.environment}"
  }
}

resource "aws_security_group" "ec2_nginx_poc" {
  name   = "${var.cluster_name}-nginx-ooyala-poc-SG"
  vpc_id = "${module.vpc.vpc_id}"

  tags = {
    "Terraform"   = "true"
    "Role"        = "Nginx instancep security group"
    "Environment" = "${var.cluster_name}"
    "Name"        = "${var.cluster_name}-ec2_nginx_poc-SG"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "allow_ssh_nginx_poc_inbound" {
  type              = "ingress"
  security_group_id = "${aws_security_group.ec2_nginx_poc.id}"

  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_http_nginx_poc_inbound" {
  type              = "ingress"
  security_group_id = "${aws_security_group.ec2_nginx_poc.id}"

  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_https_nginx_poc_inbound" {
  type              = "ingress"
  security_group_id = "${aws_security_group.ec2_nginx_poc.id}"

  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_outbound_ec2_nginx_poc" {
  type              = "egress"
  security_group_id = "${aws_security_group.ec2_nginx_poc.id}"

  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}
